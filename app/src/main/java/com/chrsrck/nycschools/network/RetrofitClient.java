package com.chrsrck.nycschools.network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * The retrofit client follows a singleton pattern so there is a general configuration for requests
 * (ex. for time out or cookie policies).
 *
 * The Retrofit client could be wrapped around a synchronized block.
 *
 * While the timeout code is in here, I did not get to conveying that through the UI by stopping
 * the spinner and displaying an error message. See the SatScore / School model classes.
 */
public class RetrofitClient {
    private static RetrofitClient instance;
    private Retrofit retrofit;
    private static final int TIMEOUT_AMOUNT = 5;

    public static RetrofitClient getInstance() {
        if (instance == null)
            instance = new RetrofitClient();
        return instance;
    }

    private RetrofitClient() {

        OkHttpClient okHttpClient =
                new OkHttpClient().newBuilder()
                        .connectTimeout(TIMEOUT_AMOUNT, TimeUnit.SECONDS)
                        .readTimeout(TIMEOUT_AMOUNT, TimeUnit.SECONDS)
                        .writeTimeout(TIMEOUT_AMOUNT, TimeUnit.SECONDS)
                        .build();

        retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl(NycPublicContract.INSTANCE.getBASE_URL())
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }
}
