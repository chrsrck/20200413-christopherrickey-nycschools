package com.chrsrck.nycschools.network;

import com.chrsrck.nycschools.model.SatScores;
import com.chrsrck.nycschools.model.School;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
The school service generates the urls that hit the Socrata end points.

 I decided to use Socrata's select feature to limit the fields relevant to
 the scope of the project.

 From the user's perspective, eliminating the
 dozens of fields returned back from school api reduces the data usage on
 their phone plan.

 From the developer's perspective, it reduces the maintenance for filtering content by moving it
 off the client to the backend.
 */
public interface SchoolService {
    // https://data.cityofnewyork.us/Education/2017-DOE-High-School-Directory/s3k6-pzi2
    @GET("s3k6-pzi2.json")
    Call<List<School>> getSchools(@Query("$select") String fields);

    // https://data.cityofnewyork.us/Education/SAT-Results/f9bf-2cp4
    @GET("f9bf-2cp4.json")
    Call<List<SatScores>> getScores(@Query("dbn") String dbn);
}
