package com.chrsrck.nycschools.network

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.chrsrck.nycschools.model.SatScores
import com.chrsrck.nycschools.model.School
import com.chrsrck.nycschools.network.NycPublicContract.limited
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * The school repository handles scheduling requests for school and sat information. After
 * scheduling a request, any activity or fragment that has access to the respostiory via a
 * viewmodel can attatch observers to that request so it can respond accordingly.
 *
 * Ex. request schools --> get live data --> live data pings the activity when it's done
 *
 * The class could be extended in a couple of ways following this guide about app architecture:
 * https://developer.android.com/jetpack/docs/guide
 *
 * 1. Wrap the data objects in another object to indicate whether a request succeed or failed / timeout
 *  aka Implement the failure method when no internet
 * 3. Add a cache so a user doesn't have to use their network for repetitive school or sat requests.
 */
object SchoolRepository {
    private val schoolService
            = RetrofitClient.getInstance().retrofit.create(SchoolService::class.java)


    fun getScores(dbn : String?) : MutableLiveData<List<SatScores>> {
        val liveData = MutableLiveData<List<SatScores>>()
        if (dbn == null)
            return liveData
        
        val callSchools = schoolService.getScores(dbn)
        callSchools.enqueue(object : Callback<List<SatScores>?> {

            override fun onResponse(call: Call<List<SatScores>?>, response: Response<List<SatScores>?>) {
                liveData.value = response.body()
            }

            override fun onFailure(call: Call<List<SatScores>?>, t: Throwable) {
                Log.d(this.javaClass.simpleName, "Get Schools Error")
            }
        })
        return liveData
    }

    fun getSchools() : MutableLiveData<List<School>> {
        val liveData = MutableLiveData<List<School>>()
        val callSchools = schoolService.getSchools(limited)
        callSchools.enqueue(object : Callback<List<School>?> {
            override fun onResponse(call: Call<List<School>?>, response: Response<List<School>?>) {
                liveData.value = response.body()
            }

            override fun onFailure(call: Call<List<School>?>, t: Throwable) {
                Log.d(this.javaClass.simpleName, "Get Schools Error")
            }
        })
        return liveData
    }
}