package com.chrsrck.nycschools.network
/*
Contract class for NYC's API.
Ensures that whatever files need certain key value pairs or api queries are consistent.


DOE: https://data.cityofnewyork.us/Education/2017-DOE-High-School-Directory/s3k6-pzi2
SAT: https://data.cityofnewyork.us/Education/2012-SAT-Results/f9bf-2cp4
 */
object NycPublicContract {

    val BASE_URL = "https://data.cityofnewyork.us/resource/"
    val SCHOOL_ENDPOINT = "s3k6-pzi2.json"
    val SAT_ENDPOINT = "f9bf-2cp4.json"

    // Query param used by the select statement for limiting school information. See SchoolService.java
    val limited = "dbn,school_name,primary_address_line_1,city,zip,state_code"
    val DBN = "dbn"
    val SCHOOL_NAME = "school_name"
    val PRIMARY_ADDRESS = "primary_address_line_1"
    val CITY = "city"
    val ZIP = "zip"
    val STATE_CODE = "state_code"
}