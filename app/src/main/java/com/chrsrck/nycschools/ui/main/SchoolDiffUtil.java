package com.chrsrck.nycschools.ui.main;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.chrsrck.nycschools.model.School;

public class SchoolDiffUtil extends DiffUtil.ItemCallback<School> {
    @Override
    public boolean areItemsTheSame(@NonNull School oldItem, @NonNull School newItem) {
        return false;
    }

    @Override
    public boolean areContentsTheSame(@NonNull School oldItem, @NonNull School newItem) {
        return false;
    }
}
