package com.chrsrck.nycschools.ui.main;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chrsrck.nycschools.R;
import com.chrsrck.nycschools.model.School;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

public class SchoolAdapter extends ListAdapter<School, SchoolAdapter.SchoolViewHolder> {

    protected SchoolAdapter(@NonNull DiffUtil.ItemCallback<School> diffCallback) {
        super(diffCallback);
    }

    @NonNull
    @Override
    public SchoolViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new SchoolViewHolder(inflater.inflate(R.layout.school_view_holder, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolViewHolder holder, int position) {
        holder.bind(getItem(position));
    }

    class SchoolViewHolder extends RecyclerView.ViewHolder {

        private TextView schoolNameTextView;
        private TextView addressTextView;
        private TextView zipTextView;
        private School mSchool;

        public SchoolViewHolder(View itemView) {
            super(itemView);
            schoolNameTextView = itemView.findViewById(R.id.school_name_text);
            addressTextView = itemView.findViewById(R.id.address_text);
            zipTextView = itemView.findViewById(R.id.zip_code_text);
            itemView.setOnClickListener(view -> {
                FragmentActivity activity = (FragmentActivity) view.getContext();
                DetailFragment fragment = DetailFragment.newInstance(mSchool.getDbn());
                activity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, fragment)
                        .addToBackStack(null) // ensures we can go back to master fragment
                        .commit();
            });
        }

        public void bind(School school) {
            mSchool = school;
            schoolNameTextView.setText(school.getSchoolName());
            addressTextView.setText(school.getFullAddress());
            zipTextView.setText(school.getZip());
        }
    }
}
