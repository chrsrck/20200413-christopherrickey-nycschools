package com.chrsrck.nycschools.ui.main;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.chrsrck.nycschools.R;
import com.chrsrck.nycschools.ui.util.VerticalDecorator;

/*
This fragment is the "Master" fragment in Master-Detail fragment paradigm.
 */
public class MainFragment extends Fragment {

    private MainViewModel mViewModel;
    private ProgressBar mProgressBar;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class); // activity lifecycle so vm is shared with detail fragment
        mProgressBar = view.findViewById(R.id.progress_bar);
        RecyclerView recyclerView = view.findViewById(R.id.school_recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        SchoolAdapter schoolAdapter = new SchoolAdapter(new SchoolDiffUtil());

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(schoolAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        Integer vertMargin = (int) getResources().getDimension(R.dimen.card_viewholder_vert_margin);
        recyclerView.addItemDecoration(new VerticalDecorator(vertMargin));
        subscribeToSchoolResponse(schoolAdapter);
    }

    /*
        This listens for the response to the school request then refreshes the recyclerview
        with that information.

        The school adapter handles all the data binding for the school model objects.
     */
    private void subscribeToSchoolResponse(SchoolAdapter schoolAdapter) {
        mViewModel.getSchoolsLiveData().observe(getViewLifecycleOwner(), schools -> {
            mProgressBar.setVisibility(View.GONE);
            schoolAdapter.submitList(schools);
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

}
