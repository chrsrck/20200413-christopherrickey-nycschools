package com.chrsrck.nycschools.ui.main;

import com.chrsrck.nycschools.model.SatScores;
import com.chrsrck.nycschools.model.School;
import com.chrsrck.nycschools.network.SchoolRepository;
import java.util.List;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

/*
The ViewModel ensures that both the master and detail fragment can use the same
instance of the school repository. It is is attatched to the main activities lifecycle.

For the school requests the app does not re-request for phone flips.

 The getSchools LiveData is public in case in the future we want a swipe to refresh
 layout.
 */
public class MainViewModel extends ViewModel {
    private final String TAG = this.getClass().getSimpleName();
    private SchoolRepository mSchoolRepository;
    private LiveData<List<School>> mSchoolLiveData;

    public MainViewModel() {
        mSchoolRepository = SchoolRepository.INSTANCE;
        mSchoolLiveData = mSchoolRepository.getSchools();
    }

    public LiveData<List<SatScores>> getSatScoresLiveData(String dbn) {
        return mSchoolRepository.getScores(dbn);
    }

    public LiveData<List<School>> getSchoolsLiveData() {
        return mSchoolLiveData;
    }
}
