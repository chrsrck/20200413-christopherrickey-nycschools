package com.chrsrck.nycschools.ui.main;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.widget.TextView;

import com.chrsrck.nycschools.R;
import com.chrsrck.nycschools.model.SatScores;

/*
The detail fragment adapter is separated away from the fragment to ensure
all the bindings are unit testable.

Some schools that have empty strings or "s" as their score.
Ex. "Archimedes Academy for math, science, and technology applications
So the app displays "N/A" for each individual score that might be messed up.
This ensures if the data is fixed for just one score (ex. reading) the other's can still display N/A
 */
public class DetailFragmentAdapter {

    private Context mContext;

    public DetailFragmentAdapter(Context context) {
        mContext = context;
    }

    public void bindSchoolName(TextView schoolTextView, String schoolName) {
        schoolTextView.setText(schoolName);
    }

    public void bindMathText(TextView mathTextView, String score) {
        mathTextView.setText(mContext.getString(R.string.math_score, getScoreText(score)));
    }

    public void bindReadingText(TextView readingTextView, String score) {
        readingTextView.setText(mContext.getString(R.string.reading_score, getScoreText(score)));
    }

    public void bindWritingText(TextView writingTextView, String score) {
        writingTextView.setText(mContext.getString(R.string.writing_score, getScoreText(score)));
    }

    private String getScoreText(String score) {
        return (TextUtils.isDigitsOnly(score)) ? score: mContext.getString(R.string.n_a_abbreviation);
    }
}
