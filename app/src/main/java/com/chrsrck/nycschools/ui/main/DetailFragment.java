package com.chrsrck.nycschools.ui.main;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.chrsrck.nycschools.R;
import com.chrsrck.nycschools.model.SatScores;
import com.chrsrck.nycschools.network.NycPublicContract;

public class DetailFragment extends Fragment {
    private static final String ARG_DBN_ID = NycPublicContract.INSTANCE.getDBN();
    private MainViewModel mViewModel;
    private String mDbnId;
    private TextView mNameText;
    private TextView mMathText;
    private TextView mReadingText;
    private TextView mWritingText;
    private ProgressBar mProgressBar;

    public DetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param dbn Dbn for school we want details for.
     * @return A new instance of fragment DetailFragment.
     */
    public static DetailFragment newInstance(String dbn) {
        DetailFragment fragment = new DetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_DBN_ID, dbn);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mDbnId = getArguments().getString(ARG_DBN_ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mViewModel = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        mNameText = view.findViewById(R.id.school_name_text);
        mMathText = view.findViewById(R.id.math_score_text);
        mReadingText = view.findViewById(R.id.reading_score_text);
        mWritingText = view.findViewById(R.id.writing_score_text);
        mProgressBar = view.findViewById(R.id.progress_bar_detail);
        DetailFragmentAdapter adapter = new DetailFragmentAdapter(this.getContext());
        subscribeToSatResponse(adapter);
    }

    /**
     * This method listens for a response from the sat score request.
     *
     *
     * The isValidScoreList could be eliminated in the future when the live data response is
     * wrapped around another object that handles connections error / timeouts / wrapping issues.
     * @param adapter
     */
    private void subscribeToSatResponse(DetailFragmentAdapter adapter) {
        mViewModel.getSatScoresLiveData(mDbnId).observe(getViewLifecycleOwner(), satScoresList -> {
            mProgressBar.setVisibility(View.GONE);
            boolean isValidScoreList =
                    satScoresList != null && !satScoresList.isEmpty() && satScoresList.get(0) != null;
            if (isValidScoreList) {
                SatScores scores = satScoresList.get(0);
                adapter.bindSchoolName(mNameText, scores.getSchoolName());
                adapter.bindMathText(mMathText, scores.getMathScore());
                adapter.bindReadingText(mReadingText, scores.getReadingScore());
                adapter.bindWritingText(mWritingText, scores.getWritingScore());
            }
            else {
                adapter.bindSchoolName(mNameText, getString(R.string.no_school_sat_information_available));
            }
        });
    }

}
