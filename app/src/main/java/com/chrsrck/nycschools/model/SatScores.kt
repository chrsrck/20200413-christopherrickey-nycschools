package com.chrsrck.nycschools.model

import com.google.gson.annotations.SerializedName

/**
 * Kotlin data class because it does not need to be extended nor have an functions implemented.
 * AKA its a POJO.
 *
 * Serializaition tags are for retrofit.
 *
 * Documentation: https://dev.socrata.com/foundry/data.cityofnewyork.us/f9bf-2cp4
 */
data class SatScores(
        @SerializedName("dbn") val dbn : String,
        @SerializedName("school_name") val schoolName : String,
        @SerializedName("num_of_sat_test_takers") val numTestTakers : String,
        @SerializedName("sat_critical_reading_avg_score")val readingScore : String,
        @SerializedName("sat_math_avg_score") val mathScore : String,
        @SerializedName("sat_writing_avg_score") val writingScore : String
)