package com.chrsrck.nycschools.model

import android.os.Parcel
import android.os.Parcelable
import android.os.Parcelable.Creator
import androidx.recyclerview.widget.DiffUtil.ItemCallback
import com.google.gson.annotations.SerializedName
import java.util.*

/*
Api documentation: https://dev.socrata.com/foundry/data.cityofnewyork.us/s3k6-pzi2

For the scope of this project, I used the Socrata API's select statement to limit the content for
use. See SchoolService.java for further explanation.

@SerializedName and parcelable implementation allow retrofit to work

Eliminating the "data class" annotation and putting the "full address" implementation into the model
prevents reimplementation of a basic method

Equals and hashcode implementation are needed for the main screen's list adapter diff util to
refresh all the viewholders.
 */

class School(
        @SerializedName("dbn") val dbn : String,
        @SerializedName("school_name") val schoolName : String,
        @SerializedName("primary_address_line_1") val addressLine1 : String,
        @SerializedName("city")val city : String,
        @SerializedName("zip") val zip : String,
        @SerializedName("state_code") val state_code : String
) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readString()?: "",
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readString() ?: "",
            parcel.readString() ?: "") {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(dbn)
        parcel.writeString(schoolName)
        parcel.writeString(addressLine1)
        parcel.writeString(city)
        parcel.writeString(zip)
        parcel.writeString(state_code)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Creator<School> {
        override fun createFromParcel(parcel: Parcel): School {
            return School(parcel)
        }

        override fun newArray(size: Int): Array<School?> {
            return arrayOfNulls(size)
        }
    }

    fun getFullAddress() : String {
        return "$addressLine1, $city, $state_code"
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false
        val school: School = o as School
        return Objects.equals(dbn, school.dbn)
    }

    override fun hashCode(): Int {
        return Objects.hash(dbn)
    }


}